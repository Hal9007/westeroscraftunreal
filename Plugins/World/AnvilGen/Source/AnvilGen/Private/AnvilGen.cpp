// Copyright 2015, WesterosCraft <http://www.westeroscraft.com>. All Rights Reserved. 

#include "AnvilGenPluginPrivatePCH.h"
#include "AnvilGen.h"
#include "BrickGridComponent.h"

UAnvilGen::UAnvilGen(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{}

int16 ScrapIDs[7] = { 6, 31, 32, 37, 38, 39, 40 };
int16 KnownIDs[18] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18 };

void UAnvilGen::TestMessage(const FAnvilGenParameters& Parameters, class UBrickGridComponent* Grid)
{
	const FInt3 BricksPerRegion = Grid->BricksPerRegion;
	FString ProjectPath = FPaths::GameDir();
	ProjectPath += "Anvil/" + Parameters.WorldName + "/";
	std::string TempPath(TCHAR_TO_UTF8(*ProjectPath));
	const char * AnvilPath = TempPath.c_str();

	struct mcmap_region *Region = mcmap_region_read(0, 0, AnvilPath);
	struct mcmap_region_chunk *NewChunk = &Region->chunks[0][0];
	struct mcmap_chunk *Chunk = mcmap_chunk_read(NewChunk, MCMAP_PARTIAL, 0);

	int TempValue = Chunk->geom->blocks[54][1][1];
	FString PrintValue = FString::FromInt(TempValue);

	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Cyan, PrintValue);
	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Cyan, TEXT("The value selected from height 54 was:"));

	FString NumX = "X: " + FString::FromInt(BricksPerRegion.X);
	FString NumY = "Y: " + FString::FromInt(BricksPerRegion.Y);
	FString NumZ = "Z: " + FString::FromInt(BricksPerRegion.Z);
	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Cyan, NumZ);
	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Cyan, NumY);
	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Cyan, NumX);
	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Cyan, TEXT("The bricks per region are:"));
}

void UAnvilGen::InitRegion(const FAnvilGenParameters& Parameters, class UBrickGridComponent* Grid, const FInt3& RegionCoordinates)
{
	const double StartTime = FPlatformTime::Seconds();

	FString ProjectPath = FPaths::GameDir();
	ProjectPath += "Anvil/" + Parameters.WorldName + "/";
	std::string TempPath(TCHAR_TO_UTF8(*ProjectPath));
	const char * AnvilPath = TempPath.c_str();

	// Region and chunk coordinates for file parsing.
	int32 RegionX = RegionCoordinates.X >> 5;
	int32 RegionZ = RegionCoordinates.Y >> 5;
	int32 ChunkX = RegionCoordinates.X - (RegionX * 32);
	int32 ChunkZ = RegionCoordinates.Y - (RegionZ * 32);

	if (ChunkX < 0 || ChunkX > 31 || ChunkZ < 0 || ChunkZ > 31)
	{
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Cyan, TEXT("The region was not initialized because the chunks were out of bounds."));
		return;
	}

	// Hook into mcmap library to parse the given coordinates.
	struct mcmap_region *Region = mcmap_region_read(RegionX, RegionZ, AnvilPath);
	if (Region == NULL)
	{
		return;
	}
	struct mcmap_region_chunk *NewChunk = &Region->chunks[ChunkX][ChunkZ];
	struct mcmap_chunk *Chunk = mcmap_chunk_read(NewChunk, MCMAP_PARTIAL, 0);
	if (Chunk == NULL)
	{
		// Deallocate the memory for the region struct.
		mcmap_region_free(Region);

		return;
	}

	// Initialize the empty array for the brick grid.
	const FInt3 BricksPerRegion = Grid->BricksPerRegion;
	TArray<uint16> LocalBrickMaterials;
	LocalBrickMaterials.Init(BricksPerRegion.X * BricksPerRegion.Y * BricksPerRegion.Z);

	const FInt3 MinRegionBrickCoordinates = RegionCoordinates * BricksPerRegion;
	const FInt3 MaxRegionBrickCoordinates = MinRegionBrickCoordinates + BricksPerRegion - FInt3::Scalar(1);

	for (int32 LocalX = 0; LocalX < BricksPerRegion.X; ++LocalX)
	{
		for (int32 LocalY = 0; LocalY < BricksPerRegion.Y; ++LocalY)
		{
			for (int32 LocalZ = 0; LocalZ < BricksPerRegion.Z; ++LocalZ)
			{
				const int32 X = MinRegionBrickCoordinates.X + LocalX;
				const int32 Y = MinRegionBrickCoordinates.Y + LocalY;
				const int32 Z = MinRegionBrickCoordinates.Z + LocalZ;
				const FInt3 BrickCoordinates(X, Y, Z);

				int32 MaterialIndex = 1;
				int Block = Chunk->geom->blocks[LocalZ][LocalX][LocalY];
				int Data = Chunk->geom->data[LocalZ][LocalX][LocalY];
				if (Block == 0)
				{
					MaterialIndex = 0;
				} 
				if (std::find(std::begin(ScrapIDs), std::end(ScrapIDs), Block) != std::end(ScrapIDs))
				{
					MaterialIndex = 0;
				}
				else
				{
					if (std::find(std::begin(KnownIDs), std::end(KnownIDs), Block) != std::end(KnownIDs))
					{
						MaterialIndex = Block;
					}
				}
				LocalBrickMaterials[((LocalY * BricksPerRegion.X) + LocalX) * BricksPerRegion.Z + LocalZ] = MaterialIndex;
			}
		}
	}

	Grid->SetBrickMaterialArray(MinRegionBrickCoordinates, MaxRegionBrickCoordinates, LocalBrickMaterials);

	// Deallocate the memory for the region and chunk structs.
	mcmap_region_free(Region);
	mcmap_chunk_free(Chunk);

	UE_LOG(LogStats, Log, TEXT("UAnvilGen::InitRegion took %fms"), 1000.0f * float(FPlatformTime::Seconds() - StartTime));
}