// Copyright 2015, WesterosCraft <http://www.westeroscraft.com>. All Rights Reserved. 

using UnrealBuildTool;
using System.Collections.Generic;

public class WesterosCraftEditorTarget : TargetRules
{
	public WesterosCraftEditorTarget(TargetInfo Target)
	{
		Type = TargetType.Editor;
	}

	//
	// TargetRules interface.
	//

	public override void SetupBinaries(
		TargetInfo Target,
		ref List<UEBuildBinaryConfiguration> OutBuildBinaryConfigurations,
		ref List<string> OutExtraModuleNames
		)
	{
		OutExtraModuleNames.Add("WesterosCraft");
	}
}
