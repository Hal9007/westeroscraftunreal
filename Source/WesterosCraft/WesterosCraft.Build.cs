// Copyright 2015, WesterosCraft <http://www.westeroscraft.com>. All Rights Reserved. 

using UnrealBuildTool;
using System.IO;

public class WesterosCraft : ModuleRules
{
	public WesterosCraft(TargetInfo Target)
	{
		PublicDependencyModuleNames.AddRange(new string[] {"Core", "CoreUObject", "Engine", "InputCore"});
	}
}
